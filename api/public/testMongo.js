module.exports = (app, config) =>
    async function testMongo() {
        return true
        return await app.withMongodb(
            async(db, client) => {
                const collection = db.collection('users')
                return collection.insertMany([{ name: 'foo' }])
            }, {
                dbName: config.dbName
            }
        )
    }