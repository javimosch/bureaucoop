module.exports = (app, config) => async() => {
    let pkg = require('sander').readFileSync(config.getPath('package.json'))
    pkg = JSON.parse(pkg.toString('utf-8'))
    return Object.keys(pkg.instances).map(key => {
        return {
            name: key,
            ...pkg.instances[key]
        }
    })
}