module.exports = async(app, config) => {
    let funql = app.funqlApi
    await funql.loadFunctionsFromFolder({
        namespace: config.name,
        path: config.getPath('api/public'),
        params: [app, config]
    })

    app.use(
        config.getRouteName(),
        require('express').static(config.getPath('public_html'))
    )
    app.get(config.getRouteName() + '*', (req, res) => {
        res.sendFile(config.getPath('public_html/index.html'))
    })
}