import Context from "./Context";
import React from "react";
export default class Provider extends React.Component {
  state = {
    city: {},
  };
  render() {
    return (
      <Context.Provider
        value={{
          city: this.state.city,
          setCity: city => {
            console.log("setCity", city);
            this.setState({
              city,
            });
          },
        }}
      >
        {this.props.children}{" "}
      </Context.Provider>
    );
  }
}
