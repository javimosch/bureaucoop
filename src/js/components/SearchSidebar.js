import React from "react";
import Context from "../Context";
import styled from "styled-components";

var Metadata = styled.p`
  word-break: break-word;
  font-size: 12px;
`;
export default class SearchSidebar extends React.Component {
  render() {
    return (
      <Context.Consumer>
        {ctx => (
          <div>
            {!!ctx.city && ctx.city.label ? <h3> {ctx.city.label.split(",")[0]} </h3> : <div></div>}

            <label> Metadata </label>
            <Metadata>{JSON.stringify(ctx.city, null, 2)}</Metadata>
          </div>
        )}
      </Context.Consumer>
    );
  }
}
