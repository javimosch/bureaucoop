import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
let FooterRoot = styled.footer`
  text-align: center;
  margin-top: 100px;
  height: 150px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export default class Footer extends React.Component {
  async componentDidMount() {}
  render() {
    return (
      <FooterRoot>
        {" "}
        Made with 💛 by&nbsp;
        <a href="https://montpedigital.misitioba.com" target="_blank">
          Montpedigital
        </a>
        &nbsp;and some fantastic contributors!{" "}
      </FooterRoot>
    );
  }
}
