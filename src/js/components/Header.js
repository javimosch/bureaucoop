import React from 'react'
import ReactDOM from 'react-dom'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const NavBrand = styled.div`
  max-width: 70%;
`
const Brand = styled.h3`
  font-size: 20px;
  @media (min-width: 580px) {
    font-size: 35px;
  }
`

const Nav = styled.nav`
  margin: 10px 15px 30px 15px;
  width: -webkit-fill-available;
`

export default class Header extends React.Component {
  async componentDidMount () {}
  render () {
    return (
      <Nav className='border  split-nav'>
        <NavBrand className='nav-brand'>
          <Brand>
            <Link to={{ pathname: '/' }}> BUREAU - COOP </Link>{' '}
          </Brand>{' '}
          <h5> Share your desk, never work alone </h5>{' '}
        </NavBrand>{' '}
        <div className='collapsible'>
          <input id='collapsible1' type='checkbox' name='collapsible1' />
          <button>
            <label htmlFor='collapsible1'>
              <div className='bar1' />
              <div className='bar2' />
              <div className='bar3' />
            </label>{' '}
          </button>{' '}
          <div className='collapsible-body'>
            <ul className='inline'>
              <li>
                <Link to={{ pathname: '/instances' }}> Instances </Link>{' '}
              </li>{' '}
            </ul>{' '}
          </div>{' '}
        </div>{' '}
      </Nav>
    )
  }
}
