import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import L from 'leaflet'
import { Helmet } from 'react-helmet'
import { OpenStreetMapProvider } from 'leaflet-geosearch'
import MapMarkers from './MapMarkers'
import Context from '../Context'
let Wrapper = styled.div``
let Map = styled.div`
  height: calc(100vh / 2);
`
let ResultItem = styled.li`
  list-style: none;
  cursor: pointer;
`
let SearchInput = styled.input`
  margin: 0 auto;
  margin-bottom: 30px;
  width: 80%;
`
export default class SearchMap extends React.Component {
  constructor () {
    super()
    this.state = {
      filterRawTypeBy: 'city',
      selectedCity: null,
      results: [],
      provider: null,
      map: null
    }
  }
  async componentDidMount () {
    function getLocation () {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition)
      } else {
        console.warn('Geolocation is not supported by this browser.')
      }
    }

    function showPosition (position) {
      let p = position.coords
      const openGeocoder = require('node-open-geocoder')
      openGeocoder()
        .reverse(p.longitude, p.latitude)
        .end((err, res) => {
          console.info(err, res)
          if (!err) {
            map.flyTo(L.latLng(res.lat, res.lon))
            // map.zoomOut(2)
          }
        })
    }

    const provider = new OpenStreetMapProvider()

    this.map.id = 'map'
    const map = new L.Map('map', {
      center: [51.505, -0.09],
      zoom: 12
    })
    L.tileLayer(
      'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
      {
        maxZoom: 18,
        attribution:
          'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
          '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
          'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
      }
    ).addTo(map)
    this.setState({
      provider,
      map
    })
    this.loadLastCity(map).catch(getLocation)
  }
  async componentDidUpdate () {}
  async search () {
    let { provider } = this.state
    this.setState({
      results: (await provider.search({
        query: this.input.value
      })).filter(r => this.state.filterRawTypeBy == r.raw.type)
    })
    console.log(this.state.results)
    this.input.value = ''
  }
  handleEnter () {
    this.search()
  }
  handleKeyDown (e) {
    if (e.keyCode === 13) {
      this.handleEnter()
    }
  }
  loadLastCity (map) {
    return new Promise((resolve, reject) => {
      try {
        let city = JSON.parse(window.localStorage.getItem('city'))
        map.flyTo(L.latLng(city.raw.lat, city.raw.lon))
        this.setState({
          selectedCity: city
        })
        resolve()
      } catch (err) {
        console.warn(err)
        reject()
      }
    })
  }
  handleResultItemClick (e, ctx) {
    console.log('CLICK', e, ctx)
    let index = e.target.dataset.index
    let result = this.state.results[index]
    let bounds = result.bounds
    this.state.map.fitBounds(result.bounds)
    this.setState({
      results: [],
      selectedCity: result
    })
    window.localStorage.setItem('city', JSON.stringify(result))
    ctx.setCity(result)
  }
  render () {
    return (
      <Wrapper>
        <Helmet>
          <link
            rel='stylesheet'
            href='https://unpkg.com/leaflet@1.5.1/dist/leaflet.css'
            integrity='sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=='
            crossorigin=''
          />

          <script
            src='https://unpkg.com/leaflet@1.5.1/dist/leaflet.js'
            integrity='sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=='
            crossorigin=''
          />
        </Helmet>{' '}
        <SearchInput
          placeholder='Which city do you want to work?'
          ref={el => (this.input = el)}
          onKeyDown={this.handleKeyDown.bind(this)}
        />{' '}
        <Context.Consumer>
          {ctx =>
            this.state.results.length === 0 ? (
              <div />
            ) : (
              <ul>
                {' '}
                {this.state.results.map((item, index) => (
                  <ResultItem
                    data-index={index}
                    onClick={evt => this.handleResultItemClick(evt, ctx)}
                    key={index}
                  >
                    {item.label}{' '}
                  </ResultItem>
                ))}{' '}
              </ul>
            )
          }
        </Context.Consumer>
        <Map
          style={{
            display: this.state.results.length === 0 ? 'block' : 'none'
          }}
          ref={el => (this.map = el)}
        />{' '}
        <MapMarkers city={this.state.selectedCity} />{' '}
      </Wrapper>
    )
  }
}
