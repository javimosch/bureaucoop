import React from 'react'
import ReactDOM from 'react-dom'
import api from '../utils/api'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
var InstanceName = styled.h3`
  margin: 10px 0px;
`
var HighlightedText = styled.h5`
  background-color: red;
  color: white;
  width: fit-content;
  margin: 0px;
`

var InstanceUrlLink = styled.a`
  font-size: 16px;
`
var InstanceItem = styled.li`
  margin-bottom: 15px;
`
export default class InstancesInfo extends React.Component {
  constructor () {
    super()
    this.state = {
      current: {},
      instances: []
    }
  }
  async getInfo () {
    let instances = await api('getInstances')
    let isLocalhost = window.location.href.indexOf('localhost') !== -1
    let current = null
    if (isLocalhost) {
      instances.push({
        name: 'localhost',
        url: window.location.origin
      })
    }
    current = instances.find(i => i.url == window.location.origin)
    return {
      current,
      instances
    }
  }
  async componentDidMount () {
    this.setState(await this.getInfo())
  }
  render () {
    let unofficialText = !this.state.current ? (
      <HighlightedText>This is an unofficial instance</HighlightedText>
    ) : (
      ''
    )
    return (
      <div>
        <h5 style={{ margin: '10px 0px' }}>
          {' '}
          You are now connected to the instance{' '}
        </h5>{' '}
        <InstanceName>
          {' '}
          {this.state.current
            ? this.state.current.name
            : window.location.origin}{' '}
        </InstanceName>{' '}
        {unofficialText}
        <h5
          style={{
            marginBottom: '0px'
          }}
        >
          {' '}
          Available Instances{' '}
        </h5>{' '}
        <ul>
          {' '}
          {this.state.instances.map(i => {
            return (
              <InstanceItem key={i.name}>
                {i.name}
                <br />
                <InstanceUrlLink href={i.url} target='_blank'>
                  {i.url}
                </InstanceUrlLink>
              </InstanceItem>
            )
          })}{' '}
        </ul>{' '}
      </div>
    )
  }
}
