import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'
import HomeView from './HomeView'
export default class RootContainer extends React.Component {
  async componentDidMount () {}
  render () {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={HomeView} />{' '}
        </Switch>{' '}
      </BrowserRouter>
    )
  }
}
