import React from "react";
import ReactDOM from "react-dom";
import Header from "../components/Header";
import Footer from "../components/Footer";
import SearchMap from "../components/SearchMap";
import { Helmet } from "react-helmet";
import SearchSidebar from "../components/SearchSidebar";
export default class HomeView extends React.Component {
  constructor() {
    super();
    this.state = {};
  }
  async componentDidMount() {}
  render() {
    return (
      <div>
        <Helmet>
          <title> Search a desk </title>
        </Helmet>
        <Header />
        <div className="row">
          <div className="sm-12 md-8 lg-9 col">
            <SearchMap />
          </div>
          <div className="sm-12 md-4 lg-3 col">
            <SearchSidebar />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
