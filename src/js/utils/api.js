import auth from './auth'
export default async function(name, args, extraOptions = {}) {
    let options = {
        name,
        args,
        ...extraOptions
    }
    let _token = localStorage.getItem('token')
    options._token = _token
    options.namespace = options.namespace || 'bureaucoop'
    return new Promise((resolve, reject) => {
        window.api
            .funql(options)
            .then(resolve)
            .catch(err => {
                if (err === 401) {
                    auth.isAuthenticated = false
                }
                reject(err)
            })
    })
}